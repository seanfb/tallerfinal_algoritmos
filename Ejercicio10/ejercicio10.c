#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ncurses.h>

//Hacer un programa que registre 30 números en un array de una dimensión que
//muestre los números registrados en las posiciones impares 
//de forma decreciente,
//sin tomar en cuenta el intervalo entre 15 y 20

void ingresoDatos();
int ValidatorNumbers();
void orden();

int array[30], arrayorden[15], i, val;
char n[15];

int main(){
	
	ingresoDatos();
	orden();

}

void ingresoDatos(){

        system("clear");
        for(i=0;i<30;i++){
                do{
                        printf("ingrese %d° numero: ", i+1);
                        scanf("%s", n);
                        val = ValidatorNumbers(n);
			array[i] = atoi(n);
			if(array[i] < 0 || val == 1)
				printf("ERROR: El dato ingresado debe ser un numero entero mayor o igual a 0.\n");
                }while(array[i] < 0 || (val == 1));
        }

        printf("\n");
}

void orden(){

	for(i=0; i<30; i++){
		if(!(i % 2 == 0) && (i < 15 || i > 20))
		       printf("El numero %d está en la posicion %d del arreglo.\n", array[i-1], i);
	}
}	

int ValidatorNumbers(char n[]){
    int i;
    for (i=0; i<strlen(n);i++) {
        if(!(isdigit(n[i]))) {
            return 1;
        }
    }
    return 0;
}





