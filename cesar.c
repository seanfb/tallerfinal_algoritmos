#include<stdio.h>
#include<stdlib.h>
#include<ncurses.h>
#include<string.h>
 
// Hacer un programa que solicite 10 edades y al finalizar se muestren en orden inverso al que fueron ingresados
 
void ingresoDatos();
int ValidatorNumbers();
 
int edad[10], i ,val;
char n[5];
 
int main(){
 
        ingresoDatos();
        ValidatorNumbers();
}
 
void ingresoDatos(){
 
        system("clear");
        for(i=0;i<10;i++){
                do{
                        printf("Ingresa la edad %d: ", i+1);
                        scanf("%s", n);
                        val = ValidatorNumbers(n);
                        edad[i] = atoi(n);
                        if(edad[i] == 0 || val ==1)
                                printf("El dato ingresado no es válido. Ingresa un valor entero mayor que cero. \n");
                }while(edad[i]<=0 || (val == 1));
        }
 
        printf("\nLas edades en orden inverso son: ");
 
        i=9;
        while(i>=0){
                printf("\n%d", edad[i]);
                i = i-1;
        }
        printf("\n");
}
 
int ValidatorNumbers(char n[]){
        int i;
        for (i=0; i<strlen(n); i++){
                if(!(isdigit(n[i]))){
                        return 1;
                }
        }
        return 0;
}
