#include<stdio.h>
#include<stdlib.h>
#include<ncurses.h>
#include<string.h>

//Hacer un programa que solicite 10 edades y al finalizar se muestren en orden inverso al que
//fueron ingresados.

void ingresoDatos();
int ValidatorNumbers();

int edad[10], i, val;
char n[5];

int main(){
	
	ingresoDatos();
}

void ingresoDatos(){

        system("clear");
        for(i=0;i<10;i++){
                do{
                        printf("ingrese edad %d: ", i+1);
                        scanf("%s", n);
                        val = ValidatorNumbers(n);
			edad[i] = atoi(n);
			if(edad[i] == 0 || val == 1)
				printf("ERROR: El dato ingresado debe ser un numero entero mayor a 0.\n");
                }while(edad[i]<=0 || val == 1);
        }

        printf("\nLas edades en orden inverso:");

        i=9;
        while(i>=0){
                printf("\n%d", edad[i]);
                i = i-1;
        }
        printf("\n");
}

int ValidatorNumbers(char n[]){
    int i;
    for (i=0; i<strlen(n);i++) {
        if(!(isdigit(n[i]))) {
            return 1;
        }
    }
    return 0;
}

