#include<stdio.h>
#include<stdlib.h>
#include<ncurses.h>
#include<string.h>

//Declaracion de funciones

void leerDulces();
void leerBebidas();
void leerConservas();
void promAnualBebidas();
void mesMayorDulces();
//void mesMayoryMenorBebidas();
void numeroMes();

//Declaracion de variables globales

int i, Dulces[12], Bebidas[12], Conservas[12], Total[12], totalbebidas, mesmayordulces, mayordulces;
char num[10];
const char * mes[12] = {"enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"};

int main(){
//Llamar a las funciones
	system("clear");
	printf("Taller Final - Algoritmos y Estructuras de Datos 2020 \n");
	printf("Analisis Costos de Producción 2019 \n\n");
	leerDulces();
	leerBebidas();
	leerConservas();
	promAnualBebidas();
	mesMayorDulces();
	//mesMayoryMenorBebidas();
	//menorCostoProduccionDic();
	//mesesMenorCostoTOTAL();
	//buscCostoTresRubrosenxMes();
	//buscCostodexRubroenyMes();
}

//Declaracion de funciones

void leerDulces(){
	printf("Primero, ingresar costos de producción por mes en 'Dulces'\n");
	for(i=0;i<12;i++){
		printf("\n%s: $", (mes[i]));
		scanf("%d", &Dulces[i]);
	}
	return;
}

void leerBebidas(){
        system("clear");
	printf("Segundo, ingresar costos de producción por mes en 'Bebidas'\n");
        for(i=0;i<12;i++){
                printf("\n%s: $", (mes[i]));
                scanf("%d", &Bebidas[i]);
        }
        return;
}

void leerConservas(){
        system("clear");
	printf("Tercero, ingresar costos de producción por mes en 'Conservas'\n");
        for(i=0;i<12;i++){
                printf("\n%s: $", (mes[i]));
                scanf("%d", &Conservas[i]);
        }
        return;
}

void promAnualBebidas(){
	system("clear");
	for(i=0;i<12;i++){
		totalbebidas = totalbebidas + Bebidas[i];
	}
	printf("\nEl promedio anual de costo para producción de 'Bebidas' es: $%d\n", (totalbebidas/12));
}

void mesMayorDulces(){
	mayordulces = 0;
	for(i=0;i<12;i++){
		if(Dulces[i] > mayordulces){
			mayordulces = Dulces[i];
			mesmayordulces = i;
		}
	}
	printf("\nEl costo mayor en producción de 'Dulces' se produjo en el mes de %s\n", (mes[mesmayordulces]));
}

//void imprimirElementos(){
//	system("clear");
//	printf("\n2.- El costo promedio anual de produccion para bebidas es: $%d\n\n", (totalbebidas/12));
//	return;	
//}

//void imprimirElementos(){

//	system("clear");
//	for(i=0; i<12; i++){
//		printf("Costos de produccion en mes %d \n", (i+1));
//	       	printf("Dulces: $%d \n", Dulces[i]);
//		printf("Bebidas: $%d \n", Bebidas[i]);
//		printf("Conservas: $%d \n", Conservas[i]);
//	}
//	return;
//}
