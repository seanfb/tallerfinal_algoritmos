#include<stdio.h>
#include<stdlib.h>
#include<ncurses.h>
#include<string.h>

// Hacer un programa que registre 30 numeros en un array de una dimension que
// muestre los numeros registrados en las posiciones impares de forma
// decreciente, sin tomar en cuenta el intervalo 15 y 20

void ingresoDatos();
int ValidatorNumbers();
void orden();

int array[30], arrayorden[15], i, val;
char n[15];

int main(){

        ingresoDatos();
        ValidatorNumbers();
        orden();
}

void ingresoDatos(){

        system("clear");
        for(i=0;i<30;i++){
                do{
                        printf("Ingresa un %d° numero: ", i+1);
                        scanf("%s", n);
                        val = ValidatorNumbers(n);
                        array[i] = atoi(n);
                        if(array[i] < 0 || val == 1)
                                printf("El dato ingresado no es válido, ingresa un número entero mayor o igual a cero. \n");
                }while(array[i] < 0 || (val == 1));
        }
        printf("\n");
}

void orden(){

        for(i=0; i<30; i++){
                if(!(i % 2 == 0) && (i < 15 || i > 20))
                        printf("El número %d se encuentre en la posición %d del arreglo. \n", array[i-1], i);
        }
}

int ValidatorNumbers(char n[]){
        int i;
        for (i=0; i<strlen(n); i++){
                if(!(isdigit(n[i]))){
                        return 1;
                }
        }
        return 0;
}
